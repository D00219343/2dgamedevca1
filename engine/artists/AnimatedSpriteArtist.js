
/**
 * Animates the associated parent by iterating across a series of animation frames
 * @author
 * @version 1.0
 * @class AnimatedSpriteArtist
 */
class AnimatedSpriteArtist
{
    constructor(context, spritesheet, framesPerSec, cells, startCellIndex=0)
    {
        this.context = context;
        this.spritesheet = spritesheet;

        //to do...
        this.frameRatePerSec = this.originalFrameRatePerSec = framesPerSec;   //10fps => 1/10 => 100ms
        this.frameIntervalInMs = 1000.0/framesPerSec; //
        this.timeSinceLastFrameInMs = 0;

        this.cells = cells;
        this.startCellIndex = startCellIndex%this.cells.length; //prevent too large value
        this.currentCellIndex = this.startCellIndex;
    }

    get Spritesheet() {return this.spritesheet;}
    get Cells() {return this.cells;}
    get StartCellIndex() {return this.startCellIndex;}
    get CurrentCellIndex() {return this.currentCellIndex;}

    set Spritesheet(spritesheet) {this.spritesheet = spritesheet;}
    set Cells(cells) {
        this.cells = cells;
        
    }
    set StartCellIndex(startCellIndex) {this.startCellIndex = startCellIndex;}
    set CurrentCellIndex(currentCellIndex) {this.currentCellIndex = currentCellIndex;}

    Update(gameTime, parent)
    {
        if(!this.paused)
        {
            this.timeSinceLastFrameInMs += Math.round(gameTime.ElapsedTimeInMs);
            if(this.timeSinceLastFrameInMs > this.frameIntervalInMs)
            {
                this.Advance();
                this.timeSinceLastFrameInMs = 0;
            }
        }
    }

    Pause() {
        this.paused = true;
    }

    Unpause() {
        this.paused = false;
    }

    Reset(){
        this.paused = false;
        this.currentCellIndex = this.startCellIndex;
        this.timeSinceLastFrameInMs = 0;
    }

    Draw(gameTime, parent)
    {
        //to do...
        this.context.save();
        //console.log(this.currentCellIndex);
        var cell = this.cells[this.currentCellIndex];
 
        var transform = parent.Transform2D;
        this.context.translate(
            transform.TranslationOffset.X, 
            transform.TranslationOffset.Y);  
        this.context.scale(transform.Scale.X, transform.Scale.Y);
        
        this.context.drawImage(this.spritesheet, 
            cell.left, cell.top, 
            cell.width, cell.height, 
            transform.Translation.X, transform.Translation.Y, //0, 0
            cell.width, cell.height);
        this.context.restore();

    }

    Advance() {

        if(this.currentCellIndex === this.cells.length - 1)
            this.currentCellIndex = 0;
        else    
            this.currentCellIndex++;
          //to do...
     }

        //to do...Equals()

    Clone()
    {
        return new AnimatedSpriteArtist(this.context, this.spritesheet, this.frameRatePerSec, this.cells, this.startCellIndex);
    }

    ToString()
    {
        return "[" + this.spritesheet + "," + this.frameRatePerSec + "," + this.cells + "," + this.startCellIndex +"]";
    }

}