class AnimLoader
{

    constructor(SheetLength, SheetHeight, Rows, LastFrameIndex, Columns)
    {
        this.SheetLength = SheetLength;
        this.SheetHeight = SheetHeight;
        this.Rows = Rows;
        this.Columns = Columns;
        this.LastFrameIndex = LastFrameIndex;
        // this.startCellIndex = startCellIndex%this.cells.length; //prevent too large value
        // this.currentCellIndex = this.startCellIndex;
    }

 LoadAnims(){

    var cells = [];
    var ACTOR_WIDTH = this.SheetLength / this.Columns;
    var ACTOR_HEIGHT = this.SheetHeight / this.Rows;
    var i = 0;
    //Cycles through the first row of animations 1-u
    for(let u = 0; u < this.SheetLength + ACTOR_WIDTH; u++){
        //When the row reaches its max(u), it adds a column(i) and resets the row u=0
        if(u == this.LastFrameIndex){
            i++;
            u=0;	
        }
    //This takes the row and column, gets the sprite at it, and adds it to the array of sprites
    cells.push(
        {
        left: ACTOR_WIDTH * u,
        top:  ACTOR_HEIGHT * i,
        width: ACTOR_WIDTH,
        height:  ACTOR_HEIGHT
        }
    );

    //When the last sprite is reached, exit the for loop by setting u > the amount of sprites
    if(i >= this.Rows && u >= this.LastFrameIndex - 1){
        u = this.ACTOR_WIDTH * this.Columns + 2;
    }
    }

    //Returns the Array that has the animations loaded into it and stored
    return cells;
 }







}