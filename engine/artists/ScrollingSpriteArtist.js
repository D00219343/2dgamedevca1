
/**
 * Performs multiple draws (left, centre, right) of a background image that enable background scrolling
 * @author
 * @version 1.0
 * @class ScrollingSpriteArtist
 */
class ScrollingSpriteArtist 
{
    constructor(context, spritesheet, 
        sourcePosition, sourceDimensions, 
        screenWidth, screenHeight)
    {
        this.context = context;
        this.spritesheet = spritesheet;
        this.sourcePosition = sourcePosition;
        this.sourceDimensions = sourceDimensions;

        //to do...
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
    }

    Update(gameTime, parent)
    {
        this.UpdateHorizontalScrolling(parent);
        //this.UpdateVerticalScrolling(parent);
    }

    /**
     * This method checks if the player has scrolled HORIZONTALLY more than 1 complete SCALED sprite WIDTH and, if true, resets the translation offset.
     * The effect of this is to allow the background to scroll infinitely along the horizontal.
     *
     * @param {Sprite} parent
     * @memberof ScrollingSpriteArtist
     */
    UpdateHorizontalScrolling(parent)
    {
        let parentTranslationOffsetX = Math.abs(parent.Transform2D.TranslationOffset.X);
        let resetScreenWidth = Math.ceil(this.screenWidth * parent.Transform2D.Scale.X/parent.ScrollSpeedMultiplier);

        //if we have moved across one complete canvas width, either left or right, then reset the offset to initial position
        if(parentTranslationOffsetX >= resetScreenWidth)
            parent.Transform2D.SetTranslationOffset(new Vector2(0, parent.Transform2D.TranslationOffset.Y));           
    }

     /**
     * This method checks if the player has scrolled VERTICALLY more than 1 complete SCALED sprite HEIGHT and, if true, resets the translation offset.
     * The effect of this is to allow the background to scroll infinitely along the horizontal.
     *
     * @param {Sprite} parent
     * @memberof ScrollingSpriteArtist
     */
    UpdateVerticalScrolling(parent)
    {
        //to do...
    }


    Draw(gameTime, parent)
    {
        this.context.save();
        var transform = parent.Transform2D;
        this.context.translate(
            transform.TranslationOffset.X * parent.ScrollSpeedMultiplier, 
            transform.TranslationOffset.Y * parent.ScrollSpeedMultiplier);
        this.context.scale(transform.Scale.X, transform.Scale.Y);

        //allows us to run left
        this.context.drawImage(this.spritesheet, 
            this.sourcePosition.X, this.sourcePosition.Y, 
            this.sourceDimensions.X, this.sourceDimensions.Y, 
            transform.Translation.X - transform.Dimensions.X, 
            transform.Translation.Y, //0, 0
            transform.Dimensions.X, 
            transform.Dimensions.Y);

        this.context.drawImage(this.spritesheet, 
            this.sourcePosition.X, this.sourcePosition.Y, 
            this.sourceDimensions.X, this.sourceDimensions.Y, 
            transform.Translation.X, transform.Translation.Y, //0, 0
            transform.Dimensions.X, transform.Dimensions.Y);

        //allows us to run right
        this.context.drawImage(this.spritesheet, 
            this.sourcePosition.X, this.sourcePosition.Y, 
            this.sourceDimensions.X, this.sourceDimensions.Y, 
            transform.Translation.X + transform.Dimensions.X, 
            transform.Translation.Y, //0, 0
            transform.Dimensions.X, 
            transform.Dimensions.Y);

        this.context.restore();
    }

    //to do...Equals()

    Clone()
    {
        //to do...
    }

    ToString()
    {
        //to do...   
    }
}