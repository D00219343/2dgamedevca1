//#region Development Diary
/*
Week 3 
------
Notes:
- Runner jump and run values are finely balanced with friction and gravity - tweak these values for your own game - see Body

To Do:
- Use 

Done:
- Added NotificationCenter instanciation in game.js to support sound, sprite removal, and game state events (e.g. inventory, ammo, pickup, health)
- Added HandleNotification() methods to MyObjectManager, SoundManager, and MyGameStateManager to listen for relevant NotificationCenter events - see PlayerBehavior::HandlePickupCollision()
- Added new AudioType to support damage and pickup category audio
- Added new ActorTypes to support ammo, pickup, inventory
- Simplified CD/CR in PlayerBehavior to check for platform, enemy, pickup collisions in separate methods
- Added Body class to support friction, velocity, and gravity
- Added MoveableSprite for sprites that will have an associated Body object
- Re-factored jump/fall physics in PlayerBehavior to reduce CPU overhead and add gravity and friction
- Added new objects in MyConstants to represent BACKGROUND_DATA and PLATFORM_DATA
- Added draw culling (dont draw sprites outside screen) to MyObjectManager::DrawAll()
- Added ScrollingBackgroundArtist and renamed to ScrollingSpriteArtist
- Added RectangleSpriteArtist
- Added initial fall code

Bugs:
- When jumping and touch platform to L or R - allows double jump?
- When platform and platform above are separated by only player height?

*/

/*
Week 2 
------
To Do:
- Add PlayerMoveBehavior::ExecuteFall()
- Add ScrollingBackgroundArtist
- Add PlatformArtist

Done:
- Added RectangleSpriteArtist
- Added initial fall code
*/

/*
Week 1
------
To Do:
- Add PlayerMoveBehavior
- Add ScrollingBackgroundArtist
- Add PlatformArtist

Done:

*/

//#endregion

//#region Global Variables
/********************************************************************* EVENT LISTENERS *********************************************************************/
//add event listener for load
window.addEventListener("load", LoadGame);

/********************************************************************* GLOBAL VARS *********************************************************************/
//get a handle to our canvas
var cvs = document.getElementById("game-canvas");
//get a handle to 3D context which allows drawing
var ctx = cvs.getContext("2d");


//stores elapsed time
var gameTime;
//assets
var spriteSheet, backgroundSpriteSheet;
var enemySheet = document.getElementById("enemy_sheet");
var playerSheet = document.getElementById("player_sheet");
var playerSheetLeft = document.getElementById("player_sheet_left");
var ActorArr = new AnimLoader(385, 93, 2, 8, 8);

//managers and notification
var objectManager;
var soundManager;
var notificationCenter;
var gameStateManager;
//...

//stores screen bounds in a rectangle
var screenRectangle;
//#endregion

/************************************************************ CORE GAME LOOP CODE UNDER THIS POINT ************************************************************/

// #region  LoadGame, Start, Animate
function LoadGame() {
  //load content
  Initialize();

  //start timer - notice that it is called only after we loaded all the game content
  Start();
}

function Start() {
  //runs in proportion to refresh rate
  this.animationTimer = window.requestAnimationFrame(Animate);
  this.gameTime = new GameTime();
}

function Animate(now) {
  this.gameTime.Update(now);
  Update(this.gameTime);
  Draw(this.gameTime);
  window.requestAnimationFrame(Animate);
}

// #endregion

// #region  Update, Draw

function Update(gameTime) {
  //update all the game sprites
  this.objectManager.Update(gameTime);

  //update game state
  this.gameStateManager.Update(gameTime);

  //#region Platform Test
  //press -> and move platform left
  if (this.keyboardManager.IsKeyDown(Keys.ArrowRight)) {
    this.objectManager.DeltaTranslationOffset = new Vector2(-1, 0);
  } else if (this.keyboardManager.IsKeyDown(Keys.ArrowLeft)) {
    this.objectManager.DeltaTranslationOffset = new Vector2(1, 0);
  }
  //#endregion
}

function Draw(gameTime) {
  //clear screen on each draw of ALL sprites (i.e. menu and game sprites)
  ClearScreen(Color.Black);

  //draw all the game sprites
  this.objectManager.Draw(gameTime);
}

function ClearScreen(color) {
  ctx.save();
  ctx.fillStyle = color;
  ctx.fillRect(0, 0, cvs.clientWidth, cvs.clientHeight);
  ctx.restore();
}

// #endregion

/************************************************************ YOUR GAME SPECIFIC UNDER THIS POINT ************************************************************/

// #region  Initialize, Load
function Initialize() {
  InitializeScreenRectangle();
  LoadAssets();
  LoadNotificationCenter();
  LoadManagers();
  LoadSprites();
  this.isPlaying = false;
}


function InitializeScreenRectangle() {
  this.screenRectangle = new Rect(
    cvs.clientLeft,
    cvs.clientTop,
    cvs.clientWidth,
    cvs.clientHeight
  );
}

function LoadNotificationCenter() {
  this.notificationCenter = new NotificationCenter();
}

function LoadManagers() {
  let debugEnabled = true;
  let scrollBoundingBoxBorder = 20;
  this.objectManager = new MyObjectManager(
    "game sprites",
    this.notificationCenter,
    this.cvs,
    this.ctx,
    debugEnabled,
    scrollBoundingBoxBorder
  );

  //checks for keyboard input
  this.keyboardManager = new KeyboardManager();

  this.gameStateManager = new MyGameStateManager("store and manage game state", this.notificationCenter);

  //audio - step 3 - instanciate the sound manager with the array of cues
  //this.soundManager = new SoundManager(audioCueArray, this.notificationCenter);

  //load other managers...
}

function LoadAssets() {
  //textures
  this.spriteSheet = document.getElementById("snailbait_sprite_sheet");
  //grass
  this.jungleSpriteSheet = document.getElementById("snailbait_jungle_tileset");
  //enemy
  this.enemy_spritesheet = document.getElementById("enemy_sheet");
  //bullets
  this.spriteBulletSheet = document.getElementById("invaders_sprite_sheet");
  //see MyConstants::BACKGROUND_DATA for background sprite sheet load
}

function LoadSprites() {
  LoadBackground();
  LoadPlatforms();
  LoadPickups();
  LoadEnemies();
  LoadPlayer();
  LoadTest();
}

function LoadTest()
{
  let artist = new SpriteArtist(
    ctx,
    this.spriteSheet,
    BULLET_TYPE_A_SPRITESHEET_ORIGIN,
    BULLET_TYPE_A_SPRITESHEET_DIMENSIONS
  );
  
  //initial translation doesnt matter since it will be instanciated at player
   let transform = new Transform2D(
    BULLET_TYPE_A_OFFSET_FROM_PLAYER,
    0,
    new Vector2(1, 1),
    new Vector2(0,0),
    BULLET_TYPE_A_SPRITESHEET_DIMENSIONS,
    4
  );
  
  let archetypalBullet = new Sprite(
    "arch bullet",
    ActorType.Platform,
    transform,
    artist,
    StatusType.IsUpdated | StatusType.IsDrawn
  ); //starts as off

    this.objectManager.Add(archetypalBullet);
}

function LoadBackground() {

  for(let i = 0; i < BACKGROUND_DATA.length; i++)
  {
    let spriteArtist = new ScrollingSpriteArtist(
      ctx,
      BACKGROUND_DATA[i].spriteSheet,
      BACKGROUND_DATA[i].sourcePosition,
      BACKGROUND_DATA[i].sourceDimensions,
      cvs.width,
      cvs.height
    );
    let transform = new Transform2D(
      BACKGROUND_DATA[i].translation,
      BACKGROUND_DATA[i].rotation,
      BACKGROUND_DATA[i].scale,
      BACKGROUND_DATA[i].origin,
      new Vector2(cvs.clientWidth, cvs.clientHeight)
    );
    this.objectManager.Add(
      new Sprite(
        BACKGROUND_DATA[i].id,
        BACKGROUND_DATA[i].actorType,
        transform,
        spriteArtist,
        StatusType.IsUpdated | StatusType.IsDrawn,
        BACKGROUND_DATA[i].scrollSpeedMultiplier,
        BACKGROUND_DATA[i].layerDepth,
      )
    );
  }

  //sort all background sprites by depth 0 (back) -> 1 (front)
  this.objectManager.SortAllByDepth(this.objectManager.BackgroundSprites, 
                      function sortAscendingDepth(a, b) {return a.LayerDepth - b.LayerDepth});
  
}

function LoadPlatforms(){

  let spriteArtist = new SpriteArtist(
    ctx,
    PLATFORM_DATA.spriteSheet,
    PLATFORM_DATA.sourcePosition,
    PLATFORM_DATA.sourceDimensions
  );

  let transform = new Transform2D(
    PLATFORM_DATA.translationArray[0],
    PLATFORM_DATA.rotation,
    PLATFORM_DATA.scale,
    PLATFORM_DATA.origin,
    PLATFORM_DATA.sourceDimensions,
    PLATFORM_DATA.explodeBoundingBoxInPixels
  );

  let platformSprite = new Sprite(
    PLATFORM_DATA.id,
    PLATFORM_DATA.actorType,
    transform,
    spriteArtist,
    StatusType.IsUpdated | StatusType.IsDrawn,
    PLATFORM_DATA.scrollSpeedMultiplier,
    PLATFORM_DATA.layerDepth,
  )

  this.objectManager.Add(platformSprite);

  let clone = null;

  for(let i = 1; i < PLATFORM_DATA.translationArray.length; i++)
  {
    clone = platformSprite.Clone();
    clone.Transform2D.Translation = PLATFORM_DATA.translationArray[i];
    this.objectManager.Add(clone);
  }

}

function LoadPickups(){
  let spriteArtist = new AnimatedSpriteArtist(
    ctx,
    this.spriteSheet,
    SAPPHIRE_CELLS_FPS,
    SAPPHIRE_CELLS, //used to access animation sprites - see MyConstants
    0
  );

  let transform = new Transform2D(
    new Vector2(530, 250),
    0,
    new Vector2(1, 1),
    new Vector2(0, 0),
    new Vector2(SAPPHIRE_CELLS_WIDTH, SAPPHIRE_CELLS_HEIGHT) //used for CD/CR rectangle - see MyConstants
  );

  let pickupSprite = new Sprite(
    "sapphire",
    ActorType.Health,
    transform,
    spriteArtist,
    StatusType.IsUpdated | StatusType.IsDrawn,
    1,
    1
  );

  //your code - does a pickup need behavior?

  this.objectManager.Add(pickupSprite);
}


function LoadEnemies() {
  let spriteArtist = new AnimatedSpriteArtist(
    ctx,
    this.enemySheet,
    ENEMY_ANIMATION_FPS,
    ENEMY_CELLS_RIGHT, //used to access animation sprites - see MyConstants
    0
  );

  let transform = new Transform2D(
    new Vector2(ENEMY_START_X_POSITION, ENEMY_START_Y_POSITION),
    0,
    new Vector2(1, 1),
    new Vector2(0, 0),
    new Vector2(ENEMY_CELLS_WIDTH, ENEMY_CELLS_HEIGHT) //used for CD/CR rectangle - see MyConstants
  );

  let enemySprite = new MoveableSprite(
    "enemy",
    ActorType.Enemy,
    transform,
    spriteArtist,
    StatusType.IsUpdated | StatusType.IsDrawn,
    1,
    1
  );

  //set performance characteristics of the body attached to the moveable sprite
  enemySprite.Body.MaximumSpeed = 3;
  enemySprite.Body.Friction = FrictionType.Normal;
  enemySprite.Body.Gravity = GravityType.Normal;
  
  enemySprite.AttachBehavior(
    new EnemyLoopBehaviour(
      new Vector2(10, 0),
      50,
      45
    )
  );
  // enemySprite.AttachBehavior(
  //   new MoveBehavior(
  //     1,
  //     1
  //   )
  // )

  //this line draws the ENEMY!!!!!
  this.objectManager.Add(enemySprite);
}

function LoadPlayer() {
//BULLET
let artist = new SpriteArtist(
  ctx,
  this.spriteSheet,
  BULLET_TYPE_A_SPRITESHEET_ORIGIN,
  BULLET_TYPE_A_SPRITESHEET_DIMENSIONS
);

//initial translation doesnt matter since it will be instanciated at player
var transform = new Transform2D(
  BULLET_TYPE_A_OFFSET_FROM_PLAYER,
  0,
  new Vector2(1, 1),
  new Vector2(0,0),
  BULLET_TYPE_A_SPRITESHEET_DIMENSIONS,
  4
);

let archetypalBullet = new Sprite(
  "arch bullet",
  ActorType.Bullet,
  transform,
  artist,
  StatusType.IsUpdated | StatusType.IsDrawn,
  3
); //starts as off
archetypalBullet.AttachBehavior(
  new MoveBehavior(BULLET_TYPE_A_MOVE_DIRECTION, BULLET_TYPE_A_MOVE_SPEED)
); //60 pixels/second


  let spriteArtist = new AnimatedSpriteArtist(
    ctx,
    this.playerSheet,
    8,
    P_CELLS_RIGHT, //used to access animation sprites for right walk - see MyConstants
    0
  );

   transform = new Transform2D(
    new Vector2(RUNNER_START_X_POSITION, RUNNER_START_Y_POSITION),
    0,
    new Vector2(1, 1),
    new Vector2(0, 0),
    new Vector2(RUNNER_CELLS_WIDTH, RUNNER_CELLS_HEIGHT) //used for CD/CR rectangle - see MyConstants
  );

  let playerSprite = new MoveableSprite(
    "player",
    ActorType.Player,
    transform,
    spriteArtist,
    StatusType.IsUpdated | StatusType.IsDrawn,
    1,
    1
  );

  //set performance characteristics of the body attached to the moveable sprite
  playerSprite.Body.MaximumSpeed = 6;
  playerSprite.Body.Friction = FrictionType.Normal;
  playerSprite.Body.Gravity = GravityType.Normal;

  playerSprite.AttachBehavior(
    new PlayerBehavior(
      this.keyboardManager,
      this.objectManager,
      RUNNER_MOVE_KEYS,
      RUNNER_RUN_VELOCITY,
      RUNNER_JUMP_VELOCITY,
      RUNNER_CELLS_LEFT,
      RUNNER_CELLS_RIGHT,
      RUNNER_CELLS_JUMP
      )
  );

  playerSprite.AttachBehavior(
    new PlayerFireBehavior(
      this.keyboardManager,
      this.soundManager,
      this.objectManager,
      BULLET_TYPE_A_FIRE_INTERVAL_IN_MS,
      archetypalBullet,
      BULLET_TYPE_A_OFFSET_FROM_PLAYER
      
    )
  );

  this.objectManager.Add(playerSprite); //add player sprite
}


/*
//old style simple coloured rectangle platforms
function LoadRectangleSpritePlatforms() {

  //artist
  let spriteArtist = new RectangleSpriteArtist(
    ctx,
    new Rect(
      PLATFORM_DATA[0].left,
      PLATFORM_DATA[0].top,
      PLATFORM_DATA[0].width,
      PLATFORM_DATA[0].height
    ),
    PLATFORM_DATA[0].lineWidth,
    PLATFORM_DATA[0].strokeStyle,
    PLATFORM_DATA[0].fillStyle,
    PLATFORM_DATA[0].opacity
  );

  //transform
  let transform2D = new Transform2D(
    new Vector2(200, 0),
    0,
    new Vector2(1, 1),
    new Vector2(0, 0),
    new Vector2(PLATFORM_DATA[0].width, PLATFORM_DATA[0].height),
  );

  //sprite
  let platformSprite = new Sprite(
    "plat1",
    ActorType.Platform,
    transform2D,
    spriteArtist,
    StatusType.IsDrawn | StatusType.IsUpdated,
    1,
    1
  );

  //add to object manager
  this.objectManager.Add(platformSprite);

  for (let i = 0; i < 3; i++) {
    //clone the first sprite
    var clone = platformSprite.Clone();
    //change Transform2D.Translation, change Transform2D.Scale
    clone.Transform2D.TranslateBy(new Vector2(150 * i, -30 * i));
    clone.Transform2D.ScaleBy(new Vector2(2, 0.5));
    //add to object manager
    this.objectManager.Add(clone);
  }
}
*/

// #endregion
