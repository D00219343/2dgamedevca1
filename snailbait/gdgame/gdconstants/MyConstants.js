

//#region Audio data 
//audio - step 2 - create array of cues with same unique IDs as were loaded in HTML file
const audioCueArray = [
	//add game specific audio cues here...
	
	new AudioCue("sound_background",
		AudioType.Background, 1, 1, false, 0),
	new AudioCue("kill_enemy",
		AudioType.Damage, 1, 1, false, 0),
	new AudioCue("damage_player",
		AudioType.Damage, 1, 1, false, 0)
	
];
//#endregion

//dimensions from PNG
const BULLET_TYPE_A_SPRITESHEET_ORIGIN = new Vector2(67,20);
const BULLET_TYPE_A_SPRITESHEET_DIMENSIONS = new Vector2(7,12);


//properties of the drawn bullet on screen
const BULLET_TYPE_A_INITIAL_TRANSLATION = new Vector2(-100000, -1110);
const BULLET_TYPE_A_OFFSET_FROM_PLAYER = new Vector2(50, 10);
//properties of the MoveBehavior associated with the bullet
const BULLET_TYPE_A_MOVE_SPEED = .7;
const BULLET_TYPE_A_MOVE_DIRECTION = new Vector2(1, 0);
const BULLET_TYPE_A_MOVE_DIRECTION_LEFT = new Vector2(-1,0);
const BULLET_TYPE_A_FIRE_INTERVAL_IN_MS = 800;  

//#region Sprite data - initial positions, dimensions
   const RUNNER_CELLS_WIDTH = 50; // in pixels
   const RUNNER_CELLS_HEIGHT = 48;
   const RUNNER_ANIMATION_FPS = 12;
   const RUNNER_START_X_POSITION = 80;
   const RUNNER_START_Y_POSITION = 0;
   const RUNNER_MOVE_KEYS = [Keys.ArrowLeft, Keys.ArrowRight, Keys.ArrowUp];
   const RUNNER_RUN_VELOCITY = .0000001;
   const RUNNER_JUMP_VELOCITY = 1;

   const PLAYER_CELLS_WIDTH = 36;
   const PLAYER_CELLS_HEIGHT = 45;

   //Enemy Spritesheet
   const ENEMY_CELLS_WIDTH = 48;
   const ENEMY_CELLS_HEIGHT = 48;
   const ENEMY_ANIMATION_FPS = 5;
   const ENEMY_START_X_POSITION = 250; 
   const ENEMY_START_Y_POSITION = 185; 
   const ENEMY_RUN_VELOCITY = 0.001;
   const ENEMY_JUMP_VELOCITY = 0.6;

   // const BAT_CELLS_HEIGHT = 34; 
   // const BEE_CELLS_HEIGHT = 50;
   // const BEE_CELLS_WIDTH  = 50;
   // const BEE_ANIMATION_FPS = 12;

   const SAPPHIRE_CELLS_HEIGHT = 30;
   const SAPPHIRE_CELLS_WIDTH  = 35;
   const SAPPHIRE_CELLS_FPS = 6;

   //other things we can add...
   const BUTTON_CELLS_HEIGHT  = 20;
   const BUTTON_CELLS_WIDTH   = 31;
   const COIN_CELLS_HEIGHT = 30;
   const COIN_CELLS_WIDTH  = 30; 

   const EXPLOSION_CELLS_HEIGHT = 62;

   const RUBY_CELLS_HEIGHT = 30;
   const RUBY_CELLS_WIDTH = 35;


   const SNAIL_BOMB_CELLS_HEIGHT = 20;
   const SNAIL_BOMB_CELLS_WIDTH  = 20;
   const SNAIL_CELLS_HEIGHT = 34;
   const SNAIL_CELLS_WIDTH  = 64;
//#endregion

//#region Cell data 
   
   // const BEE_CELLS = [
   //    { left: 5,   top: 234, width:  BEE_CELLS_WIDTH,
   //                          height: BEE_CELLS_HEIGHT },

   //    { left: 75,  top: 234, width:  BEE_CELLS_WIDTH, 
   //                          height: BEE_CELLS_HEIGHT },

   //    { left: 145, top: 234, width:  BEE_CELLS_WIDTH, 
   //                          height: BEE_CELLS_HEIGHT }
   // ];
   
   const BACKGROUND_CELL = [
      { left: 0,   top: 590, width: 1104, height: 400 }
   ];

   //ENEMY Spritesheet (Walking right)
   const ENEMY_CELLS_RIGHT = [
      { left: 0, top: 0, 
        width: ENEMY_CELLS_WIDTH, height: ENEMY_CELLS_HEIGHT },

      { left: 48, top: 0, 
         width: ENEMY_CELLS_WIDTH, height: ENEMY_CELLS_HEIGHT },

      { left: 96, top: 0, 
         width: ENEMY_CELLS_WIDTH, height: ENEMY_CELLS_HEIGHT },

      { left: 144, top: 0, 
         width: ENEMY_CELLS_WIDTH, height: ENEMY_CELLS_HEIGHT },

      { left: 192, top: 0, 
         width: ENEMY_CELLS_WIDTH, height: ENEMY_CELLS_HEIGHT },

      { left: 240, top: 0, 
         width: ENEMY_CELLS_WIDTH, height: ENEMY_CELLS_HEIGHT },

      { left: 288,  top: 0, 
         width: ENEMY_CELLS_WIDTH, height: ENEMY_CELLS_HEIGHT },

      { left: 336,  top: 0, 
         width: ENEMY_CELLS_WIDTH, height: ENEMY_CELLS_HEIGHT },

      { left: 384,   top: 0, 
         width: ENEMY_CELLS_WIDTH, height: ENEMY_CELLS_HEIGHT },
   ];

   //ENEMY Spritesheet (Walking left)
   const ENEMY_CELLS_LEFT = [
      { left: 0, top: 48,
        width: 48, height: ENEMY_CELLS_HEIGHT },

      { left: 48, top: 48,
        width: 96, height: ENEMY_CELLS_HEIGHT },

      { left: 96, top: 48,
      width: 144, height: ENEMY_CELLS_HEIGHT },

      { left: 144, top: 48,
        width: 192, height: ENEMY_CELLS_HEIGHT },

      { left: 192, top: 48,
        width: 240, height: ENEMY_CELLS_HEIGHT },

      { left: 240, top: 48,
        width: 288, height: ENEMY_CELLS_HEIGHT },

      { left: 288, top: 48,
        width: 336, height: ENEMY_CELLS_HEIGHT },

      { left: 336, top: 48,
        width: 384, height: ENEMY_CELLS_HEIGHT },

      { left: 384, top: 48,
        width: 432, height: ENEMY_CELLS_HEIGHT },
   ];

   
   const RUNNER_CELLS_LEFT = [
      { left: 0, top: 200, 
         width: 36, height: PLAYER_CELLS_HEIGHT },

      { left: 50, top: 200, 
         width: 36, height: PLAYER_CELLS_HEIGHT },

      { left: 100,  top: 200, 
         width: 46, height: PLAYER_CELLS_HEIGHT },

      { left: 150,  top: 200, 
         width: 36, height: PLAYER_CELLS_HEIGHT },

      { left: 200, top: 200, 
         width: 35, height: PLAYER_CELLS_HEIGHT },

      { left: 250, top: 200, 
         width: 36, height: PLAYER_CELLS_HEIGHT },

      { left: 300, top: 200, 
         width: 36, height: PLAYER_CELLS_HEIGHT },

      { left: 350, top: 200, 
         width: 36, height: PLAYER_CELLS_HEIGHT },

      { left: 0, top: 250, 
         width: 36, height: PLAYER_CELLS_HEIGHT },

      { left: 50, top: 250, 
         width: 36, height: PLAYER_CELLS_HEIGHT },
   ];

   // const RUNNER_CELLS_JUMP = [
   //    {
   //       left: 0,
   //       top: 450,
   //       width: 35, 
   //       height: RUNNER_CELLS_HEIGHT
   //    },
   // ];

   const RUNNER_CELLS_JUMP = [
      { left: 0, top: 400, 
        width: 45, height: RUNNER_CELLS_HEIGHT },

        { left: 50, top: 400, 
         width: 45, height: RUNNER_CELLS_HEIGHT },

         { left: 100, top: 400, 
            width: 45, height: RUNNER_CELLS_HEIGHT },

            { left: 150, top: 400, 
               width: 45, height: RUNNER_CELLS_HEIGHT },

               { left: 200, top: 400, 
                  width: 45, height: RUNNER_CELLS_HEIGHT },

                  { left: 250, top: 400, 
                     width: 45, height: RUNNER_CELLS_HEIGHT },

                     { left: 300, top: 400, 
                        width: 45, height: RUNNER_CELLS_HEIGHT },

                        // { left: 350, top: 400, 
                        //    width: 45, height: RUNNER_CELLS_HEIGHT },

      // { left: 362, top: 385, 
      //    width: 44, height: RUNNER_CELLS_HEIGHT },

      // { left: 314, top: 385, 
      //    width: 39, height: RUNNER_CELLS_HEIGHT },

      // { left: 265, top: 385, 
      //    width: 46, height: RUNNER_CELLS_HEIGHT },

      // { left: 205, top: 385, 
      //    width: 49, height: RUNNER_CELLS_HEIGHT },

      // { left: 150, top: 385, 
      //    width: 46, height: RUNNER_CELLS_HEIGHT },

      // { left: 96,  top: 385, 
      //    width: 46, height: RUNNER_CELLS_HEIGHT },

      // { left: 45,  top: 385, 
      //    width: 35, height: RUNNER_CELLS_HEIGHT },

      // { left: 0,   top: 385, 
      //    width: 35, height: RUNNER_CELLS_HEIGHT }
   ];



   const P_CELLS_RIGHT = [

      // { left: 0, top: 0, 
      //    width: 10, height: PLAYER_CELLS_HEIGHT },

      // { left: 59, top: 0, 
      //    width: 18, height: PLAYER_CELLS_HEIGHT },

      // { left: 104, top: 0, 
      //    width: 27, height: PLAYER_CELLS_HEIGHT },

      // { left: 152, top: 0, 
      //    width: 38, height: PLAYER_CELLS_HEIGHT },

      // { left: 201, top: 0, 
      //    width: 34, height: PLAYER_CELLS_HEIGHT },

      // { left: 250, top: 0, 
      //    width: 36, height: PLAYER_CELLS_HEIGHT },

      { left: 300,  top: 0, 
         width: 36, height: PLAYER_CELLS_HEIGHT },

      { left: 350,  top: 0, 
         width: 35, height: PLAYER_CELLS_HEIGHT },

      { left: 0, top: 50, 
         width: 36, height: PLAYER_CELLS_HEIGHT },

      { left: 200, top: 50, 
         width: 35, height: PLAYER_CELLS_HEIGHT },

      { left: 250, top: 50, 
         width: 36, height: PLAYER_CELLS_HEIGHT },

      { left: 300, top: 50, 
         width: 36, height: PLAYER_CELLS_HEIGHT },

      { left: 350, top: 50, 
         width: 36, height: PLAYER_CELLS_HEIGHT }
      // { left: 45,  top: 5, 
      //    width: 35, height: PLAYER_CELLS_HEIGHT },

      // { left: 0,   top: 5, 
      //    width: 35, height: PLAYER_CELLS_HEIGHT }
   ];

   const P_CELLS_LEFT = [
      { left: 0,  top: 0, 
         width: 36, height: PLAYER_CELLS_HEIGHT }
   ];

   const RUNNER_CELLS_RIGHT = [
      { left: 0, top: 200, 
         width: 36, height: PLAYER_CELLS_HEIGHT },

      { left: 50, top: 200, 
         width: 36, height: PLAYER_CELLS_HEIGHT },

      { left: 100,  top: 200, 
         width: 46, height: PLAYER_CELLS_HEIGHT },

      { left: 150,  top: 200, 
         width: 36, height: PLAYER_CELLS_HEIGHT },

      { left: 200, top: 200, 
         width: 35, height: PLAYER_CELLS_HEIGHT },

      { left: 250, top: 200, 
         width: 36, height: PLAYER_CELLS_HEIGHT },

      { left: 300, top: 200, 
         width: 36, height: PLAYER_CELLS_HEIGHT },

      { left: 350, top: 200, 
         width: 36, height: PLAYER_CELLS_HEIGHT },

      { left: 0, top: 250, 
         width: 36, height: PLAYER_CELLS_HEIGHT },

      { left: 50, top: 250, 
         width: 36, height: PLAYER_CELLS_HEIGHT },
   ];

   const RUNNER_FIRE_CELLS = [
      {
         left: 300, top: 250,
         width: 36, height: PLAYER_CELLS_HEIGHT
      }
   ];

   const SAPPHIRE_CELLS = [
      { left: 185,   top: 138, width:  SAPPHIRE_CELLS_WIDTH,
                             height: SAPPHIRE_CELLS_HEIGHT },

      { left: 220,  top: 138, width:  SAPPHIRE_CELLS_WIDTH, 
                             height: SAPPHIRE_CELLS_HEIGHT },

      { left: 258,  top: 138, width:  SAPPHIRE_CELLS_WIDTH, 
                             height: SAPPHIRE_CELLS_HEIGHT },

      { left: 294, top: 138, width:  SAPPHIRE_CELLS_WIDTH, 
                             height: SAPPHIRE_CELLS_HEIGHT },

      { left: 331, top: 138, width:  SAPPHIRE_CELLS_WIDTH, 
                             height: SAPPHIRE_CELLS_HEIGHT }
   ];

   // const BAT_CELLS = [
   //    { left: 3,   top: 0, width: 36, height: BAT_CELLS_HEIGHT },
   //    { left: 41,  top: 0, width: 46, height: BAT_CELLS_HEIGHT },
   //    { left: 93,  top: 0, width: 36, height: BAT_CELLS_HEIGHT },
   //    { left: 132, top: 0, width: 46, height: BAT_CELLS_HEIGHT },
   // ];

   // const BAT_RED_EYE_CELLS = [
   //    { left: 185, top: 0, 
   //      width: 36, height: BAT_CELLS_HEIGHT },

   //    { left: 222, top: 0, 
   //      width: 46, height: BAT_CELLS_HEIGHT },

   //    { left: 273, top: 0, 
   //      width: 36, height: BAT_CELLS_HEIGHT },

   //    { left: 313, top: 0, 
   //      width: 46, height: BAT_CELLS_HEIGHT },
   // ];
   
   const BLUE_COIN_CELLS = [
      { left: 5, top: 540, width:  COIN_CELLS_WIDTH, 
                           height: COIN_CELLS_HEIGHT },

      { left: 5 + this.COIN_CELLS_WIDTH, top: 540,
       width: COIN_CELLS_WIDTH, 
        height: COIN_CELLS_HEIGHT }
   ];

   const EXPLOSION_CELLS = [
      { left: 3,   top: 48, 
        width: 52, height: EXPLOSION_CELLS_HEIGHT },
      { left: 63,  top: 48, 
        width: 70, height: EXPLOSION_CELLS_HEIGHT },
      { left: 146, top: 48, 
        width: 70, height: EXPLOSION_CELLS_HEIGHT },
      { left: 233, top: 48, 
        width: 70, height: EXPLOSION_CELLS_HEIGHT },
      { left: 308, top: 48, 
        width: 70, height: EXPLOSION_CELLS_HEIGHT },
      { left: 392, top: 48, 
        width: 70, height: EXPLOSION_CELLS_HEIGHT },
      { left: 473, top: 48, 
        width: 70, height: EXPLOSION_CELLS_HEIGHT }
   ];

   const GOLD_COIN_CELLS = [
      { left: 65, top: 540, width:  COIN_CELLS_WIDTH, 
                            height: COIN_CELLS_HEIGHT },
      { left: 96, top: 540, width:  COIN_CELLS_WIDTH, 
                            height: COIN_CELLS_HEIGHT },
      { left: 128, top: 540, width:  COIN_CELLS_WIDTH, 
                             height: COIN_CELLS_HEIGHT },
   ];

   const RUBY_CELLS = [
      { left: 3,   top: 138, width:  RUBY_CELLS_WIDTH,
                             height: RUBY_CELLS_HEIGHT },

      { left: 39,  top: 138, width:  RUBY_CELLS_WIDTH, 
                             height: RUBY_CELLS_HEIGHT },

      { left: 76,  top: 138, width:  RUBY_CELLS_WIDTH, 
                             height: RUBY_CELLS_HEIGHT },

      { left: 112, top: 138, width:  RUBY_CELLS_WIDTH, 
                             height: RUBY_CELLS_HEIGHT },

      { left: 148, top: 138, width:  RUBY_CELLS_WIDTH, 
                             height: RUBY_CELLS_HEIGHT }
   ];

   const SNAIL_BOMB_CELLS = [
      { left: 40, top: 512, width: 30, height: 20 },
      { left: 2, top: 512, width: 30, height: 20 }
   ];

   const SNAIL_CELLS = [
      { left: 142, top: 466, width:  SNAIL_CELLS_WIDTH,
                             height: SNAIL_CELLS_HEIGHT },

      { left: 75,  top: 466, width:  SNAIL_CELLS_WIDTH, 
                             height: SNAIL_CELLS_HEIGHT },

      { left: 2,   top: 466, width:  SNAIL_CELLS_WIDTH, 
                             height: SNAIL_CELLS_HEIGHT },
   ]; 
//#endregion

//#region Platform data

   //the 5 scollable backgrounds - see game.js\LoadBackgrounds()
  const BACKGROUND_DATA = [
     {
      id: "background_1",
           spriteSheet: document.getElementById("stage"),
           sourcePosition: new Vector2(2340,1008),
           sourceDimensions: new Vector2(5412, 1264),
           translation: new Vector2(0,0),
           rotation: 0,
           scale: new Vector2(10,5),
           origin: new Vector2(0,0),
           actorType: ActorType.Background,
           layerDepth: 0,
           scrollSpeedMultiplier: .25
     },
     {
     id: "background_2",
     spriteSheet: document.getElementById("stage"),
     sourcePosition: new Vector2(5,168),
     sourceDimensions: new Vector2(7688, 220),
     translation: new Vector2(0,0),
     rotation: 0,
     scale: new Vector2(10,1),
     origin: new Vector2(0,0),
     actorType: ActorType.Background,
     layerDepth: 0,
     scrollSpeedMultiplier: 1
    
     
   }
   ];
//    {
//      id: "background_1",
//      spriteSheet: document.getElementById("snailbait_background_1"),
//      sourcePosition: new Vector2(0,0),
//      sourceDimensions: new Vector2(384, 216),
//      translation: new Vector2(0,0),
//      rotation: 0,
//      scale: new Vector2(1,1),
//      origin: new Vector2(0,0),
//      actorType: ActorType.Background,
//      layerDepth: 0,
//      scrollSpeedMultiplier: 0.2
//    },
//    {
//       id: "background_2",
//       spriteSheet: document.getElementById("snailbait_background_2"),
//       sourcePosition: new Vector2(0,0),
//       sourceDimensions: new Vector2(384, 216),
//       translation: new Vector2(0,0),
//       rotation: 0,
//       scale: new Vector2(1,1),
//       origin: new Vector2(0,0),
//       actorType: ActorType.Background,
//       layerDepth: 0.1,
//       scrollSpeedMultiplier: 0.3
//     },
//     {
//       id: "background_3",
//       spriteSheet: document.getElementById("snailbait_background_3"),
//       sourcePosition: new Vector2(0,0),
//       sourceDimensions: new Vector2(384, 216),
//       translation: new Vector2(0,0),
//       rotation: 0,
//       scale: new Vector2(1,1),
//       origin: new Vector2(0,0),
//       actorType: ActorType.Background,
//       layerDepth: 0.15,
//       scrollSpeedMultiplier: 0.5
//     },
//     {
//       id: "background_4",
//       spriteSheet: document.getElementById("snailbait_background_4"),
//       sourcePosition: new Vector2(0,0),
//       sourceDimensions: new Vector2(384, 216),
//       translation: new Vector2(0,0),
//       rotation: 0,
//       scale: new Vector2(1,1),
//       origin: new Vector2(0,0),
//       actorType: ActorType.Background,
//       layerDepth: 0.2,
//       scrollSpeedMultiplier: 0.7
//     },
//     {
//       id: "background_5",
//       spriteSheet: document.getElementById("snailbait_background_5"),
//       sourcePosition: new Vector2(0,0),
//       sourceDimensions: new Vector2(384, 216),
//       translation: new Vector2(0,0),
//       rotation: 0,
//       scale: new Vector2(1,1),
//       origin: new Vector2(0,0),
//       actorType: ActorType.Background,
//       layerDepth: 0.25,
//       scrollSpeedMultiplier: 0.8
//     },
//  ];
const BULLET_DATA = {
   
};



 const PLATFORM_DATA = {
      id: "platform",
      spriteSheet: document.getElementById("blank"),
      sourcePosition: new Vector2(0, 0),
      sourceDimensions: new Vector2(500, 30),
      rotation: 0,
      scale: new Vector2(1,1),
      origin: new Vector2(0,0),
      actorType: ActorType.Platform,
      layerDepth: 0,
      scrollSpeedMultiplier: 1,
      explodeBoundingBoxInPixels: -6,
      translationArray: [new Vector2(30, 235), (new Vector2(330, 235)), (new Vector2(870, 170)), (new Vector2(1400, 200)), (new Vector2(1800, 200))],

      // id: "platform_2",
      // spriteSheet: document.getElementById("snailbait_jungle_tileset"),
      // sourcePosition: new Vector2(0, 0),
      // sourceDimensions: new Vector2(48, 2000),
      // rotation: 90,
      // scale: new Vector2(1,1),
      // origin: new Vector2(0,0),
      // actorType: ActorType.Platform,
      // layerDepth: 0,
      // scrollSpeedMultiplier: 1,
      // explodeBoundingBoxInPixels: -6,
      // translationArray: [new Vector2(30, 30)]
};

//#endregion