class EnemyLoopBehaviour
{
    currentMoveIncrement = 0;
    timeSinceLastMoveInMs = 0;
    moveDirection = 1; //1 = right, -1 = left

    constructor(moveVector, maxMoveIncrements, intervalBetweenInMs) 
    {
        this.moveVector = moveVector;
        this.maxMoveIncrements = maxMoveIncrements; //Number
        this.intervalBetweenInMs = intervalBetweenInMs; //Number
    }
 

    Execute(gameTime, parent)
    {
        this.timeSinceLastMoveInMs += gameTime.ElapsedTimeInMs;

        if(this.timeSinceLastMoveInMs >= this.intervalBetweenInMs)
        {
            this.currentMoveIncrement++;

            // if(this.currentMoveIncrement <= this.maxMoveIncrements)
            // {
            //     parent.Transform2D.Translation.Add(Vector2.MultiplyScalar(this.moveVector,  this.moveDirection));
            //     this.moveDirection *= -1;
            //     this.currentMoveIncrement = 0;
            //     this.timeSinceLastMoveInMs = 0;
            //     this.timeSinceLastMoveInMs = -this.intervalBetweenInMs;
            // }

            if(this.currentMoveIncrement <= this.maxMoveIncrements)
            {
                parent.Transform2D.TranslateBy(Vector2.MultiplyScalar(this.moveVector,  this.moveDirection));
                this.timeSinceLastMoveInMs = 0;
            }
            else if(this.currentMoveIncrement > this.maxMoveIncrements)
            {
                this.moveDirection *= -1;
                this.currentMoveIncrement = 0;
                this.timeSinceLastMoveInMs = 0;
                this.timeSinceLastMoveInMs = -this.intervalBetweenInMs;
            }
        }
    }
}